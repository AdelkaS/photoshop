# Import Module
from tkinter import *
# pip install tk
from PIL import ImageTk, Image
# pip install Pillow
from Upravy_obrazku import Upravovac


# třída, která vytvoří celý vnitřek okna. Dědíme ze třídy Tk
class App(Tk):

    # konstruktor
    def __init__(self, pocet_radku=14):
        # zavolání konstruktoru rodiče
        super().__init__()
        # nastavení pomocných proměnných
        # self.option_add('*Font', 'Verdana 12')

        self.pocet_radku = pocet_radku
        self.upravovac = Upravovac()
        self.obrazek = None
        self.obrazek2 = None

        # obecné nastavení okna
        self.geometry("1200x700")
        self.title("Photoshop")
        # zavolání metody na tvorbu horního menu
        self.nastav_menu()

        # nastavení mřížky
        # sloupce
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=1)
        self.grid_columnconfigure(3, weight=12)
        # řádky
        for i in range(self.pocet_radku):
            self.grid_rowconfigure(i, weight=1)

        # nastavení hlavní části okna
        self.nastav_leve_menu()
        self.nastav_obrazkovou_cast()

    # funkce, která nastaví celé horní menu
    def nastav_menu(self):
        # prvotní nastavení menu
        self.hlavni_menu = Menu()
        # nastavení caskádového menu
        menu_moznosti = Menu(self.hlavni_menu, tearoff=0)
        menu_moznosti.add_command(label="Načíst obrázek", command=self.nacti_obrazek)
        menu_moznosti.add_command(label="Uložit obrázek", command=self.uloz_obrazek)
        menu_moznosti.add_separator()
        menu_moznosti.add_command(label="Text pod oddělovačem")
        # přidání kaskádového menu a dalších příkazů do menu
        self.hlavni_menu.add_cascade(label="Možnosti", menu=menu_moznosti)
        self.hlavni_menu.add_command(label="Konec", command=quit)
        self.config(menu=self.hlavni_menu)


    # funkce na načtení nového obrázku
    def nacti_obrazek(self):
        self.obrazek = Image.open("color.jpg")
        # úprava velikosti obrázku, aby se zachovaly poměry stran
        self.obrazek.thumbnail((self.x, self.y))
        self.x2, self.y2 = self.obrazek.size
        # zobrazení obrázku na obrázkouvou část okna
        self.obrazek_zobraz = ImageTk.PhotoImage(self.obrazek)
        self.canvas.create_image(self.x / 2 - self.x2 / 2, self.y / 2 - self.y2 / 2, image=self.obrazek_zobraz,
                                     anchor=NW)

    # funkce na uložení obrázku
    def uloz_obrazek(self):
        if self.obrazek is not None:
            self.obrazek.save("result.jpg")

    # funkce, která vytvoří všechny ovládací prvky, které mohou
    # upravovat obrázek (menu v levé části obrazovky)
    def nastav_leve_menu(self):
        # popisek a tlačítko pro změnu obrázku na černobílý
        self.vytvor_label(0, 0, "Zmeň obrázek na černobílý")
        self.vytvor_tlacitko(1, 0, 2, "Proveď!", self.uprav_obrazek_cernobily)

        # popisek a posuvník pro změnu červené složky obrázku
        self.vytvor_label(0, 1, "Červená")
        self.posuvnik_cervena_minula_hodnota = 255
        self.vytvor_posuvnik(1, 1, 0, 512, self.posuvnik_cervena_minula_hodnota, self.uprav_obrazek_cervena_slozka)

        # popisek a posuvník pro změnu zelené složky obrázku
        self.vytvor_label(0, 2, "Zelená")
        self.posuvnik_zelena_minula_hodnota = 255
        self.vytvor_posuvnik(1, 2, 0, 512, self.posuvnik_zelena_minula_hodnota, self.uprav_obrazek_zelena_slozka)

        # popisek a posuvník pro změnu modré složky obrázku
        self.vytvor_label(0, 3, "Modrá")
        self.posuvnik_modra_minula_hodnota = 255
        self.vytvor_posuvnik(1, 3, 0, 512, self.posuvnik_modra_minula_hodnota, self.uprav_obrazek_modra_slozka)


    def vytvor_label(self, sloupec, radek, text):
        label = Label(self, text=text, wraplength=100)
        label.grid(column=sloupec, row=radek, sticky=NSEW)

    def vytvor_posuvnik(self, sloupec, radek, od, do, default, funkce):
        posuvnik = Scale(self, from_=od, to=do, orient=HORIZONTAL, command=funkce, showvalue=False)
        posuvnik.set(default)
        posuvnik.grid(column=sloupec, row=radek, columnspan=2, sticky=EW, padx=5, pady=5)

    def vytvor_tlacitko(self, sloupec, radek, pocet_sloupcu, text, funkce):
        tlacitko = Button(self, text=text, command=funkce)
        tlacitko.grid(column=sloupec, row=radek, columnspan=pocet_sloupcu, sticky=NSEW, padx=5, pady=5)


    # funkce, která nastaví část, kde se zobrazuje obrázek
    def nastav_obrazkovou_cast(self):
        # vytvoření canvasu na vykreslení a uložení jeho velikosti
        self.canvas = Canvas(self)
        self.canvas.grid(column=3, row=0, rowspan=self.pocet_radku, sticky=NSEW)
        self.canvas.update()
        self.y = self.canvas.winfo_height()
        self.x = self.canvas.winfo_width()
        #self.nacti_obrazek()


    def uprav_obrazek_cernobily(self):
        # zalování funkce na upravení obrázku a jeho zobrazení
        if self.obrazek is not None:
            self.obrazek = self.upravovac.pixelove_upravy(self.obrazek.copy(), self.upravovac.na_cernobilo, 0)
            self.zobraz_obrazek(self.obrazek)

    def uprav_obrazek_cervena_slozka(self, values):
        if self.obrazek is not None:
            zmena = int(values) - self.posuvnik_cervena_minula_hodnota
            self.posuvnik_cervena_minula_hodnota = int(values)
            self.obrazek = self.upravovac.pixelove_upravy(self.obrazek.copy(), self.upravovac.cervena_slozka, zmena)
            self.zobraz_obrazek(self.obrazek)

    def uprav_obrazek_zelena_slozka(self, values):
        if self.obrazek is not None:
            zmena = int(values) - self.posuvnik_zelena_minula_hodnota
            self.posuvnik_zelena_minula_hodnota = int(values)
            self.obrazek = self.upravovac.pixelove_upravy(self.obrazek.copy(), self.upravovac.zelena_slozka, zmena)
            self.zobraz_obrazek(self.obrazek)

    def uprav_obrazek_modra_slozka(self, values):
        if self.obrazek is not None:
            zmena = int(values) - self.posuvnik_modra_minula_hodnota
            self.posuvnik_modra_minula_hodnota = int(values)
            self.obrazek = self.upravovac.pixelove_upravy(self.obrazek.copy(), self.upravovac.modra_slozka, zmena)
            self.zobraz_obrazek(self.obrazek)


    # funkce, která zobrazí obrázek
    def zobraz_obrazek(self, obrazek_na_zobrazeni):
        self.obrazek_zobraz = ImageTk.PhotoImage(obrazek_na_zobrazeni)
        self.canvas.create_image(self.x / 2 - self.x2 / 2, self.y / 2 - self.y2 / 2, image=self.obrazek_zobraz,
                                 anchor=NW)


if __name__ == "__main__":
    app = App()
    app.mainloop()
