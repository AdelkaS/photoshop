from PIL import Image, ImageEnhance, ImageFilter

# třída na jednoduché úpravy obrázku
class Upravovac:

    def na_cernobilo(self, x, y, obrazek, zmena):
        r, g, b = obrazek.getpixel((x, y))
        prumer = int((r + g + b) / 3)
        obrazek.putpixel((x, y), (prumer, prumer, prumer))
        # obrazek = obrazek.convert("L")

    def cervena_slozka(self, x, y, obrazek, zmena):
        r, g, b = obrazek.getpixel((x, y))
        obrazek.putpixel((x, y), (r + zmena, g, b))

    def zelena_slozka(self, x, y, obrazek, zmena):
        r, g, b = obrazek.getpixel((x, y))
        obrazek.putpixel((x, y), (r, g + zmena, b))

    def modra_slozka(self, x, y, obrazek, zmena):
        r, g, b = obrazek.getpixel((x, y))
        obrazek.putpixel((x, y), (r, g, b + zmena))

    def pixelove_upravy(self, obrazek, funkce, zmena):
        sirka, vyska = obrazek.size
        for x in range(sirka):
            for y in range(vyska):
                funkce(x, y, obrazek, zmena)
        return obrazek

    def zrcadlove_otoceni(self, obrazek):
        obrazek = obrazek.transpose(Image.FLIP_LEFT_RIGHT)
        return obrazek

    def otoceni_obrazku(self, obrazek, stupne_otoceni):
        obrazek = obrazek.rotate(stupne_otoceni)
        return obrazek

    def rozmazani_1(self, obrazek):
        obrazek = obrazek.filter(ImageFilter.BLUR)
        return obrazek

    def rozmazani_2(self, obrazek):
        obrazek = obrazek.filter(ImageFilter.BoxBlur(5))
        return obrazek

    def rozmazani_3(self, obrazek):
        obrazek = obrazek.filter(ImageFilter.GaussianBlur(5))
        return obrazek
